FROM odoo:14.0

# COPY odoo.conf /etc/odoo/

COPY src/addons/ /usr/lib/python3/dist-packages/odoo/addons/

ENTRYPOINT ["/entrypoint.sh"]
CMD ["odoo"]